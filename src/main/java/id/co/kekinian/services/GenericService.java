package id.co.kekinian.services;

import id.co.kekinian.domain.RandomCity;
import id.co.kekinian.domain.User;

import java.util.List;

public interface GenericService {

    User findByUsername(String username);

    List<User> findAllUsers();

    List<RandomCity> findAllRandomCities();

}
