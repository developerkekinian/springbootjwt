package id.co.kekinian.repository;

import id.co.kekinian.domain.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
}
